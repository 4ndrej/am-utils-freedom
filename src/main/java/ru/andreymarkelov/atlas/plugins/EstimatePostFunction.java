package ru.andreymarkelov.atlas.plugins;

import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;

public class EstimatePostFunction extends AbstractJiraFunctionProvider {
    private final Logger log = Logger.getLogger(EstimatePostFunction.class);

    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final JiraDurationUtils jiraDurationUtils;

    public EstimatePostFunction(
            CustomFieldManager customFieldManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        this.customFieldManager = customFieldManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.jiraDurationUtils = ComponentAccessor.getComponentOfType(JiraDurationUtils.class);
    }

    @Override
    public void execute(
            @SuppressWarnings("rawtypes") Map transientVars,
            @SuppressWarnings("rawtypes") Map args,
            PropertySet ps) throws WorkflowException {
        MutableIssue issue = getIssue(transientVars);
        String customFieldId = (String) args.get("customFieldId");
        String inhours = (String) args.get("inhours");

        if (StringUtils.isBlank(customFieldId)) {
            return;
        }

        try {
            Long.parseLong(customFieldId);
        } catch (NumberFormatException nex) {
            log.error("Set Original Estimation: invalid function parameters");
            throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.set-estimate-function.error"));
        }

        CustomField customField = customFieldManager.getCustomFieldObject(Long.parseLong(customFieldId));
        if (customField != null) {
            Object customFieldValue = issue.getCustomFieldValue(customField);
            if (customFieldValue == null) {
                throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.set-estimate-function.error"));
            }

            long originalEstimate;
            if (NumberUtils.isNumber(customFieldValue.toString())) {
                originalEstimate = Long.parseLong(customFieldValue.toString());
                if (originalEstimate <= 0) {
                    log.error("Set Original Estimation: estimation time is negative");
                    throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.set-estimate-function.error"));
                }
            } else {
                try {
                    originalEstimate = jiraDurationUtils.parseDuration(customFieldValue.toString(), jiraAuthenticationContext.getLocale());
                } catch (InvalidDurationException e) {
                    log.error("Set Original Estimation: incorrect value");
                    throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.set-estimate-function.error"));
                }
            }

            if (Boolean.parseBoolean(inhours)) {
                originalEstimate *= 3600;
            } else {
                originalEstimate *= 60;
            }

            issue.setOriginalEstimate(originalEstimate);
            issue.setEstimate(originalEstimate);
        } else {
            log.error("Set Original Estimation: configured custom field does not exist");
            throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.set-estimate-function.error"));
        }
    }
}
