package ru.andreymarkelov.atlas.plugins;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;

import ru.andreymarkelov.atlas.plugins.amutils.workflow.function.AssignPostFunctionFactory;

public class AssignPostFunction extends AbstractJiraFunctionProvider {
    private final CustomFieldManager customFieldManager;
    private final UserManager userManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public AssignPostFunction(CustomFieldManager customFieldManager, UserManager userManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.customFieldManager = customFieldManager;
        this.userManager = userManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        MutableIssue issue = getIssue(transientVars);

        String cfId = (String) args.get(AssignPostFunctionFactory.CUSTOMFIELD);
        if (StringUtils.isEmpty(cfId)) {
            return;
        }

        CustomField customField = customFieldManager.getCustomFieldObject(Long.parseLong(cfId));
        if (customField == null) {
            return;
        }

        Object customFieldValue = issue.getCustomFieldValue(customField);
        if (customFieldValue == null) {
            throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.assign-pf.error.value", customField.getName()));
        }

        String userName;
        if (customFieldValue instanceof ApplicationUser) {
            userName = ((ApplicationUser) customFieldValue).getName();
        } else if (customFieldValue instanceof User) {
            userName = ((User) customFieldValue).getName();
        } else if (customFieldValue instanceof Project) {
            userName = ((Project) customFieldValue).getLeadUserName();
        } else {
            if (customFieldValue.toString().contains(":")) {
                userName = customFieldValue.toString().substring(0, customFieldValue.toString().indexOf(":"));
            } else {
                userName = customFieldValue.toString();
            }
        }

        ApplicationUser user = userManager.getUserByName(userName);
        if (user == null) {
            throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.assign-pf.error.nouser", customField.getName()));
        }

        issue.setAssignee(user);
    }
}
