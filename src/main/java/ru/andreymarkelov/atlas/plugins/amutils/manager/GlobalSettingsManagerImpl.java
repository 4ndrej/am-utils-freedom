package ru.andreymarkelov.atlas.plugins.amutils.manager;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class GlobalSettingsManagerImpl implements GlobalSettingsManager {
    private static final String PLUGIN_KEY = "PLUGIN_AM_UTILS_GLOBAL";
    private static final String PROJECTS = "PROJECTS";
    private static final String VAL_SEPARATOR = "&";

    private final PluginSettings pluginSettings;

    public GlobalSettingsManagerImpl(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createSettingsForKey(PLUGIN_KEY);
    }

    @Override
    public List<Long> getAttachmentListenerProjectIds() {
        List<Long> projectsIds = new ArrayList<>();
        Object storedValue = getPluginSettings().get(PROJECTS);
        if (storedValue != null) {
            StringTokenizer st = new StringTokenizer(storedValue.toString(), VAL_SEPARATOR);
            while (st.hasMoreTokens()) {
                projectsIds.add(Long.valueOf(st.nextToken()));
            }
        }
        return projectsIds;
    }

    private synchronized PluginSettings getPluginSettings() {
        return pluginSettings;
    }

    @Override
    public void setAttachmentListenerProjectsIds(List<Long> projectsIds) {
        StringBuilder sb = new StringBuilder();
        if (projectsIds != null) {
            for (Long projectsId : projectsIds) {
                sb.append(projectsId).append(VAL_SEPARATOR);
            }
        }
        getPluginSettings().put(PROJECTS, sb.toString());
    }
}
