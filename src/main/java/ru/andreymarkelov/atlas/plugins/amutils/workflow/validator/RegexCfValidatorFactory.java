package ru.andreymarkelov.atlas.plugins.amutils.workflow.validator;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class RegexCfValidatorFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public RegexCfValidatorFactory(
            CustomFieldManager customFieldManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        this.customFieldManager = customFieldManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    private Map<String, String> getCustomFields() {
        Map<String, String> map = new TreeMap<>();
        for (CustomField field : customFieldManager.getCustomFieldObjects()) {
            map.put(field.getId(), field.getName());
        }
        return map;
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> conditionParams) {
        Map<String, Object> map = new HashMap<>();

        if (conditionParams != null && conditionParams.containsKey("cfId") &&
                conditionParams.containsKey("regex") && conditionParams.containsKey("msg")) {
            String cfId = extractSingleParam(conditionParams, "cfId");
            String regex = extractSingleParam(conditionParams, "regex");
            String msg = extractSingleParam(conditionParams, "msg");

            if (isNotBlank(cfId)) {
                map.put("cfId", cfId);
            } else {
                map.put("cfId", "");
            }

            if (isNotBlank(regex)) {
                map.put("regex", regex);
            } else {
                map.put("regex", "");
            }

            if (isNotBlank(msg)) {
                map.put("msg", msg);
            } else {
                map.put("msg", "");
            }
        } else {
            map.put("cfId", "");
            map.put("regex", "");
            map.put("msg", "");
        }

        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        String value = (String) ((ValidatorDescriptor) descriptor).getArgs().get(param);
        return isNotBlank(value) ? value : "";
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("customFields", getCustomFields());
        velocityParams.put("cfId", getParam(descriptor, "cfId"));
        velocityParams.put("regex", getParam(descriptor, "regex"));
        velocityParams.put("msg", getParam(descriptor, "msg"));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put("customFields", getCustomFields());
        velocityParams.put("cfId", "");
        velocityParams.put("regex", "");
        velocityParams.put("msg", "");
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        String cfId = getParam(descriptor, "cfId");
        String regex = getParam(descriptor, "regex");
        String msg = getParam(descriptor, "msg");

        CustomField field = customFieldManager.getCustomFieldObject(cfId);
        String cfView = (field != null) ? field.getFieldName() : cfId;

        if (isEmpty(msg)) {
            msg = jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.regex-validator.error", regex);
        }

        velocityParams.put("cfId", cfId);
        velocityParams.put("cfView", cfView);
        velocityParams.put("regex", regex);
        velocityParams.put("msg", msg);
    }
}
