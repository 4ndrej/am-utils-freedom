package ru.andreymarkelov.atlas.plugins.amutils.field;

import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import org.apache.commons.validator.routines.EmailValidator;

public class EmailCf extends GenericTextCFType {
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public EmailCf(
            CustomFieldValuePersister customFieldValuePersister,
            GenericConfigManager genericConfigManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager);
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public void validateFromParams(
            CustomFieldParams customFieldParams,
            ErrorCollection errorCollection,
            FieldConfig fieldConfig) {
        super.validateFromParams(customFieldParams, errorCollection, fieldConfig);
        if (errorCollection.hasAnyErrors()) {
            return;
        }

        String value = getValueFromCustomFieldParams(customFieldParams);
        if (value == null) {
            return;
        }

        if (!EmailValidator.getInstance().isValid(value)) {
            errorCollection.addError(
                    fieldConfig.getCustomField().getId(),
                    jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.field.email.error"));
        }
    }
}
