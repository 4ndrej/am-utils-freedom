package ru.andreymarkelov.atlas.plugins.amutils.functions;

import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import static com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory.getInstance;
import static org.apache.commons.lang3.StringUtils.lowerCase;

public class UserCommentedIssuesJqlFunction extends AbstractJqlFunction {
    private final static Log log = LogFactory.getLog(UserCommentedIssuesJqlFunction.class);

    private final static String SQL = "SELECT ISSUEID FROM jiraaction WHERE ACTIONTYPE = 'comment' AND UPDATED > ? AND UPDATEAUTHOR = ? ORDER BY UPDATED DESC";

    private final UserManager userManager;
    private final PermissionManager permissionManager;
    private final IssueManager issueManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public UserCommentedIssuesJqlFunction(
        UserManager userManager,
        PermissionManager permissionManager,
        IssueManager issueManager,
        JiraAuthenticationContext jiraAuthenticationContext) {
        this.userManager = userManager;
        this.permissionManager = permissionManager;
        this.issueManager = issueManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.ISSUE;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 2;
    }

    @Override
    public List<QueryLiteral> getValues(QueryCreationContext context, FunctionOperand operand, TerminalClause terminalClause) {
        if (operand.getArgs().size() != 2) {
            return null;
        }

        String user = operand.getArgs().get(0);
        String time = operand.getArgs().get(1);

        long lastFindTime = System.currentTimeMillis();
        if ("startOfWeek".equals(time)) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_WEEK, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.clear(Calendar.MINUTE);
            cal.clear(Calendar.SECOND);
            cal.clear(Calendar.MILLISECOND);
            lastFindTime = cal.getTimeInMillis();
        } else if ("startOfDay".equals(time)) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.clear(Calendar.MINUTE);
            cal.clear(Calendar.SECOND);
            cal.clear(Calendar.MILLISECOND);
            lastFindTime = cal.getTimeInMillis();
        } else {
            try {
                long diffTime = DateUtils.getDuration(time);
                lastFindTime -= (diffTime * 1000);
            } catch (InvalidDurationException e) {
                return null;
            }
        }

        ApplicationUser applicationUser = userManager.getUserByNameEvenWhenUnknown(user);
        if (applicationUser == null) {
            applicationUser = userManager.getUserByKeyEvenWhenUnknown(user);
            if (applicationUser == null) {
                return null;
            }
        }

        List<QueryLiteral> literals = new LinkedList<>();
        try (Connection connection = getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL)) {
            preparedStatement.setTimestamp(1, new Timestamp(lastFindTime));
            preparedStatement.setString(2, lowerCase(applicationUser.getKey()));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Long issueId = resultSet.getLong(1);
                    Issue issue = issueManager.getIssueObject(issueId);
                    if ((issue != null) && permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, context.getApplicationUser())) {
                        literals.add(new QueryLiteral(operand, issueId));
                    }
                }
            }
        } catch (DataAccessException|SQLException e) {
            log.error("UserCommentedIssuesJqlFunction::getValues - An error occured", e);
            return null;
        }
        return literals;
    }

    @Override
    public MessageSet validate(ApplicationUser searcher, FunctionOperand operand, TerminalClause terminalClause) {
        MessageSet messages = new MessageSetImpl();

        List<String> keys = operand.getArgs();
        if (keys.size() != 2) {
            messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.jql.usercommentedissues.incorrectparameters", "userCommentedIssues"));
        } else {
            String user = keys.get(0);
            String time = keys.get(1);

            ApplicationUser userObj = userManager.getUserByKeyEvenWhenUnknown(user);
            if (userObj == null) {
                messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("utils.incorrectuserparameter", operand.getName()));
            } else {
                if ("startOfWeek".equals(time) || "startOfDay".equals(time)) {
                    //--> nothing
                } else {
                    try {
                        DateUtils.getDuration(time);
                    } catch (InvalidDurationException e) {
                        messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.jql.usercommentedissues.incorrectparameters", "userCommentedIssues"));
                    }
                }
            }
        }

        return messages;
    }
}
