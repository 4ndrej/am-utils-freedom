package ru.andreymarkelov.atlas.plugins.amutils.functions;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class AreNotBlockedBy extends AbstractJqlFunction {
    private static final Logger logger = Logger.getLogger(AreNotBlockedBy.class);

    private String[] FUNCTION_ARGS = {"Open", "Reopened", "In Progress", "is blocked by"};

    private final SearchService searchService;

    public AreNotBlockedBy(SearchService searchService) {
        this.searchService = searchService;
    }

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.ISSUE;
    }

    public List<Issue> getLinkedIssues(ApplicationUser user) throws SearchException {
        JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
        queryBuilder.where().issue().inFunc("linkedIssuesHasStatuses", FUNCTION_ARGS);
        Query query = queryBuilder.buildQuery();
        List<Issue> issues = searchService.search(user, query, PagerFilter.getUnlimitedFilter()).getIssues();
        return issues;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 0;
    }

    @Override
    public List<QueryLiteral> getValues(QueryCreationContext queryContext, FunctionOperand functionOperand, TerminalClause terminalClause) {
        List<QueryLiteral> result = new LinkedList<>();
        try {
            List<Issue> issues = getLinkedIssues(queryContext.getApplicationUser());
            for (Issue issue : issues) {
                result.add(new QueryLiteral(functionOperand, issue.getId()));
            }
        } catch (SearchException ex) {
            logger.error("Error during search: " + ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public MessageSet validate(ApplicationUser user, FunctionOperand functionOperand, TerminalClause terminalClause) {
        MessageSetImpl messageSet = new MessageSetImpl();
        if (!functionOperand.getArgs().isEmpty()) {
            messageSet.addErrorMessage("Should by 0 arguments in function " + getFunctionName());
        }
        return messageSet;
    }
}
